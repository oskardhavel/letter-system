import * as React from 'react'

import Table from 'react-bootstrap/Table'
import Button from 'react-bootstrap/Button'
import 'bootstrap/dist/css/bootstrap.min.css'

export class Home extends React.Component {
  public render() {
    return (
      <span>Home</span>
    )
  }
}

class UserDetails extends React.Component<any, any> {
  public render() {
    const user: UserDetail = this.props.user
    return (
      <tr>
        <td>{user.name}</td>
        <td>{user.surname}</td>
        <td>{user.city}</td>
        <td>
          <Button>Rasyti</Button>
        </td>
      </tr>
    )
  }
}

interface UserDetail {
  name: string
  surname: string
  city: string
  id: Number
  email: string
}

function fetchUsers(): UserDetail[] {
  return [
    {
      name: "Povlas",
      surname: "Savickas",
      city: "Klaipeda",
      id: 2525252525,
      email: "test@gmail.com"
    },
    {
      name: "Gustas",
      surname: "Labutis",
      city: "Klaipeda",
      id: 252525,
      email: "gustas@gmail.coma"
    }
  ]
}

export class Mails extends React.Component {
  public render() {
    const users = fetchUsers()
    return (
      <div className="mails">
      <Table striped>
        <thead>
          <tr>
            <th>Vardas</th>
            <th>Pavarde</th>
            <th>Miestas</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          { users && users.map(user2 => {
            return (
              <UserDetails user={user2} />
            )
          }) }
        </tbody>
      </Table>
      </div>
    )
  }
}

export class NewMail extends React.Component<any, any> {
  userId: Number = 0
  constructor(props: any) {
    super(props)
    this.userId = props.match.params.userId
  }

  public render() {
    return (
      <span>Mails - {this.userId}</span>
    )
  }
}
