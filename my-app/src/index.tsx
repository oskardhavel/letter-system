import React from 'react'
import ReactDOM from 'react-dom'
import * as serviceWorker from './serviceWorker'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import * as Components from './components'

ReactDOM.render(
  <BrowserRouter>
  <div className="app">
    <Switch>
      <Route exact path="/" component={Components.Home} />
      <Route path="/laiskai" component={Components.Mails} />
      <Route path="/newMail/:userId" component={Components.NewMail} />
    </Switch>
  </div>
  </BrowserRouter>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
